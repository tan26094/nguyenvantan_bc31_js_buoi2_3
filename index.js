// Bài 1: Tính tiền lương nhân viên
/**
 * Input:
 * workdays=24;
 * salary=100000;
 *
 * Process:
 * salary=workdays*daysalary
 *
 * Output:
 * salary=?
 *
 *
 *
 *
 */

var salary = 0;

function tinhluong() {
  var daysalary = document.getElementById("daysalary").value * 1;
  var workdays = document.getElementById("workdays").value * 1;
  salary = workdays * daysalary;
  console.log(salary);
  document.getElementById("salary").innerHTML =
    "<P>Tiền lương là: " + salary + " VND</P>";
}

// -------------------------------------------------------------------------------
// Bài 2: Tính tính giá trị trung bình
/**
 * Input:
 *
 * real1=1
 * real2=2
 * real3=3
 * real4=4
 * real5=5
 *
 * Process:
 * average=(real1+real2+real3+real4+real5)/5
 *
 * Output:
 * average=?
 *
 */

var average = 0;

function tinhtb() {
  var real1 = document.getElementById("num1").value * 1;
  var real2 = document.getElementById("num2").value * 1;
  var real3 = document.getElementById("num3").value * 1;
  var real4 = document.getElementById("num4").value * 1;
  var real5 = document.getElementById("num5").value * 1;
  average = (real1 + real2 + real3 + real4 + real5) / 5;
  console.log(average);
  document.getElementById("average").innerHTML =
    "<P>giá trị trung bình của 5 số là: " + average + "</P>";
}

console.log("giá trị trung bình của 5 số là: ", average);
// -------------------------------------------------------------------------------
// Bài 3: Quy đổi tiền
/**
 * Input:
 *USD=23500
 *usdamount=100
 *
 * Process:
 * US2VND=USD*usdamount
 *
 * Output:
 * US2VND=?
 *
 */

var US2VND = 0;
const USD = 23500;
function doitien() {
  var usdamount = document.getElementById("txt-usd").value * 1;

  US2VND = usdamount * USD;
  t = new Intl.NumberFormat("vn-VN").format(US2VND);
  console.log(t);
  document.getElementById("US2VND").innerHTML =
    "<P>" + usdamount + " USD tương đương với " + t + "VND</P>";
}

// -------------------------------------------------------------------------------
// Bài 4: Tính diện tích chu vi hình chữ nhật
/**
 * Input:
 *dai=10
 *rong=5
 *
 * Process:
 * dientich=dai*rong
 * chuvi=(dai+rong)*2
 * Output:
 * dientich=?
 * chivi=?
 *
 */

var chuvi = null;
var dientich = null;

function tinhchuvidientich() {
  var dai = document.getElementById("txt-dai").value * 1;
  var rong = document.getElementById("txt-rong").value * 1;

  dientich = dai * rong;
  chuvi = (dai + rong) * 2;

  document.getElementById("cvdt").innerHTML =
    "<P>Chu vi là" + chuvi + ", diện tích là: " + dientich + "</P>";
}
console.log("Diện tích là:", dientich, "m");
console.log("Chu vi là", chuvi, "m");
// -------------------------------------------------------------------------------
// Bài 5: Tính tổng hai ký số
/**
 * Input:
 *num=12
 *
 *
 * Process:
 * donvi=num%10
 * chuc=num/10
 * tong=chuc+donvi
 * Output:
 * tong=?
 *
 */

var chuc = null;
var donvi = null;

function tinhkyso() {
  var num = document.getElementById("txt-number").value * 1;

  chuc = Math.floor(num / 10);
  donvi = num % 10;
  tong = chuc + donvi;

  console.log("Tổng hai ký số là:", tong);
  document.getElementById("kyso").innerHTML =
    "<P>Tổng ký số của số " + num + " là: " + tong + "</P>";
}
